#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from datetime import datetime, timedelta, date
from trytond.pool import PoolMeta, Pool
from trytond.model import ModelView, ModelSQL, fields
from trytond.report import Report
from trytond.pool import Pool
from trytond.wizard import Wizard, StateAction, StateView, Button
from trytond.transaction import Transaction


class StaffContract(metaclass=PoolMeta):
    __name__ = 'staff.contract'

    @classmethod
    def notification_contracts_to_expire(cls):
        pool = Pool()
        Template = pool.get('email.template')
        today_ = date.today()
        date_expired = today_ + timedelta(days=35)
        contracts = cls.search([
            ('state', '=', 'active'),
            ('end_date', '>', today_),
            ('end_date', '<=', date_expired),
        ])
        if contracts:
            template, = Template.search(['id', '=', 10])
            Template.send(template, contracts)

    def get_days_to_expire(self):
        return (self.end_date - date.today()).days


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'
    # documents = fields.One2Many('staff.document', 'employee', 'Documents')
    notification_documents = fields.One2Many('notification.document', 'origin',
                                             string='Notification Documents')

    @classmethod
    def __setup__(cls):
        super(Employee, cls).__setup__()


class NotificationDocument(metaclass=PoolMeta):
    __name__ = 'notification.document'

    @classmethod
    def _get_origin(cls):
        return super(NotificationDocument, cls)._get_origin() + ['company.employee']


# class Document(ModelSQL, ModelView):
#     'Staff Document'
#     __name__ = 'staff.document'
#     employee = fields.Many2One('company.employee', 'Employee')
#     kind = fields.Many2One('staff.kind_document', 'Kind', required=True)
#     date_start = fields.Date('Date Start')
#     date_end = fields.Date('Date End')
#     description = fields.Text("Description")
#
#     @classmethod
#     def __setup__(cls):
#         super(Document, cls).__setup__()


class DocumentExpiryDateStart(ModelView):
    'Document Expiry Date Start'
    __name__ = 'staff.print_document_expiry_date.start'
    limit_date = fields.Date('Limit Date')
    company = fields.Many2One('company.company', 'Company', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class DocumentExpiryDate(Wizard):
    'Document Expiry Date'
    __name__ = 'staff.print_document_expiry_date'
    start = StateView('staff.print_document_expiry_date.start',
            'staff_document.print_document_expiry_date_start_view_form', [
                Button('Cancel', 'end', 'tryton-cancel'),
                Button('Print', 'print_', 'tryton-print', default=True),
                ])
    print_ = StateAction('staff_document.report_document_expiry_date')

    def do_print_(self, action):
        data = {
                'limit_date': self.start.limit_date,
                'company': self.start.company.id,
                }
        return action, data

    def transition_print_(self):
        return 'end'


class DocumentExpiryDateReport(Report):
    'Document Expiry Date Report'
    __name__ = 'staff.document.expiry_date_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)

        Document = Pool().get('notification.document')
        Company = Pool().get('company.company')
        report_context['limit_date'] = data['limit_date']
        report_context['company'] = Company(data['company'])
        dom_documents = []
        if data['limit_date']:
            dom_documents.extend([
                ('origin', 'LIKE', 'company_employee%'),
                ('expire_date', '<=', data['limit_date']),
                ('expire_date', '>=', data['limit_date'] - datetime.timedelta(30)),
            ])
        report_context['records'] = Document.search(dom_documents)
        return report_context
